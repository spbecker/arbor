# Copyright 2010-2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'rpcbind-0.2.0.ebuild' from Gentoo, which is:
#     Copyright 1999-2009 Gentoo Foundation

require sourceforge systemd-service \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="Universal Addresses to RPC Program Number Mapper"
DESCRIPTION="
The rpcbind utility is a server that converts RPC program numbers
into universal addresses.
"
HOMEPAGE+=" https://git.linux-nfs.org/?p=steved/${PN}.git"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~arm ~x86"
MYOPTIONS="systemd tcpd"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
    build+run:
        net-libs/libtirpc
        tcpd? ( sys-apps/tcp-wrappers )
        systemd? ( sys-apps/systemd )
    run:
        user/rpc
        group/rpc
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-1.2.5-exherbo-environment.patch
    "${FILES}"/${PN}-1.2.5-runstatdir.patch
    "${FILES}"/${PN}-1.2.5-systemd-tmpfiles.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --bindir=/usr/$(exhost --target)/bin
    --enable-warmstarts
    --disable-rmtcalls
    --with-nss-modules="files"
    --with-statedir=/run/${PN}
    --with-rpcuser=rpc
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    "systemd systemdsystemunitdir ${SYSTEMDSYSTEMUNITDIR}"
    "systemd systemdtmpfilesdir /usr/$(exhost --target)/lib/tmpfiles.d"
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    "tcpd libwrap"
)

src_install() {
    default

    insinto /etc/conf.d
    doins "${FILES}"/systemd/rpcbind.conf
}

