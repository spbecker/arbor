# Copyright 2007 Bryan Østergaard <kloeri@exherbo.org>
# Copyright 2012 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

export_exlib_phases src_install

MY_PNV=${PN}-$(ever range 1-3)
require alternatives gnu [ suffix=tar.xz ]

SUMMARY="Multi-precision floating point library"
HOMEPAGE="http://www.${PN}.org"

LICENCES="LGPL-3"
SLOT=$(ever major)
MYOPTIONS="parts: development documentation libraries"

DEPENDENCIES="
    build+run:
        dev-libs/gmp:=[>=4.1]
"

WORK=${WORKBASE}/${MY_PNV}

DEFAULT_SRC_CONFIGURE_PARAMS=( --enable-shared )

mpfr_src_install() {
    default

    edo pushd "${IMAGE}" &> /dev/null
    # replace libmpfr.so for an alt-light on libmpfr.so.${sover}
    local sofile=usr/$(exhost --target)/lib/libmpfr.so
    local solink=$(edo readlink "${sofile}")
    local sover=${solink##*.so.}
    edo rm "${sofile}"
    alternatives_for _$(exhost --target)_${PN} ${SLOT} ${SLOT} \
        /"${sofile}" /"${sofile}".${sover%%.*}

    # other files get moved and slotted
    for ifile in usr/$(exhost --target)/include/*.h \
                 usr/$(exhost --target)/lib/libmpfr.a \
                 usr/$(exhost --target)/lib/libmpfr.la \
                 usr/$(exhost --target)/lib/pkgconfig/mpfr.pc \
                 usr/share/info/mpfr.info; do
        [ -f "${ifile}" ] || continue
        # ifile=foo/bar.x  =>  ifile_s=foo/bar-${SLOT}.x
        local ifile_s=${ifile%.*}-${SLOT}.${ifile##*.}
        alternatives_for _$(exhost --target)_${PN} ${SLOT} ${SLOT} /"${ifile}" /"${ifile_s}"
    done
    edo popd & >/dev/null

    expart development /usr/$(exhost --target)/include
    expart documentation /usr/share/{doc,info}
    expart libraries /usr/$(exhost --target)/lib
}

