# Copyright 2017 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PV=$(ever replace_all -)

SUMMARY="German, Austrian-German and Swiss-German language dictionary for MySpell and Hunspell"
HOMEPAGE="
    https://extensions.libreoffice.org/extensions/german-de-at-frami-dictionaries
    https://extensions.libreoffice.org/extensions/german-de-de-frami-dictionaries
    https://extensions.libreoffice.org/extensions/german-de-ch-frami-dictionaries
"
DOWNLOADS="
    https://extensions.libreoffice.org/extensions/german-de-de-frami-dictionaries/${MY_PV}/@@download/file/dict-de_DE-frami_${MY_PV}.oxt
    https://extensions.libreoffice.org/extensions/german-de-at-frami-dictionaries/$(ever replace 2 . ${MY_PV})/@@download/file/dict-de_AT-frami_${MY_PV}.oxt
    https://extensions.libreoffice.org/extensions/german-de-ch-frami-dictionaries/$(ever replace 2 . ${MY_PV})/@@download/file/dict-de_CH-frami_${MY_PV}.oxt
"

LICENCES="GPL-2 LGPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/unzip
"

WORK=${WORKBASE}

src_unpack() {
    default

    for i in AT CH DE ; do
        edo unzip -qo "${FETCHEDDIR}"/dict-de_${i}-frami_${MY_PV}.oxt -d "${WORKBASE}"
    done
}

src_install() {
    dodir /usr/share/myspell

    for i in AT CH DE ; do
        # Spell checking
        insinto /usr/share/hunspell
        newins de_${i}_frami/de_${i}_frami.aff de_${i}.aff
        newins de_${i}_frami/de_${i}_frami.dic de_${i}.dic
        dosym /usr/share/hunspell/de_${i}.aff /usr/share/myspell/de_${i}.aff
        dosym /usr/share/hunspell/de_${i}.dic /usr/share/myspell/de_${i}.dic

        # Hyphenation
        insinto /usr/share/hyphen
        doins hyph_de_${i}/hyph_de_${i}.dic
        dosym /usr/share/hyphen/hyph_de_${i}.dic /usr/share/myspell/hyph_de_${i}.dic

        # Thesaurus
        insinto /usr/share/mythes
        doins thes_de_${i}_v2/th_de_${i}_v2.dat
        doins thes_de_${i}_v2/th_de_${i}_v2.idx
        dosym /usr/share/mythes/th_de_${i}_v2.dat /usr/share/myspell/th_de_${i}_v2.dat
        dosym /usr/share/mythes/th_de_${i}_v2.idx /usr/share/myspell/th_de_${i}_v2.idx
    done

    emagicdocs
}

